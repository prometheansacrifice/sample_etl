from setuptools import setup

setup(name='sample_etl',
      version='0.1',
      description='Sample ETL script',
      url='',
      author='John Doe',
      author_email='doe@john.com',
      license='MIT',
      packages=['sample_etl'],
      zip_safe=False) 
